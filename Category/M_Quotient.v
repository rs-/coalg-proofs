Require Import Logic.Eqdep.
Require Import Relations.Relation_Operators.
Require Import Category.Types.
Require Import Category.Setoids.
Require Import Category.Types_Setoids.
Require Import Category.CoAlg.

Require Import Theory.Category.
Require Import Theory.Functor.
Require Import Theory.Product.
Require Import Theory.Isomorphism.
Require Import Theory.InitialTerminal.
Require Import Theory.CoAlgebra.

Require Import FunctionalExtensionality.

Generalizable All Variables.

Import EqNotations.
Notation ρ₁ := projT1.
Notation ρ₂ := projT2.

Lemma rew_concat : ∀ {A} {P : A → Type} {a b c : A} (p : a = b) (q : b = c) (i : P a),
                     rew q in rew p in i = rew (eq_trans p q) in i.
Proof.
  intros A P a b c p. destruct p. intro q. destruct q. auto.
Qed.

(*------------------------------------------------------------------------------
  -- Ｍ  ＥＮＤＯＦＵＮＣＴＯＲ
  ----------------------------------------------------------------------------*)

Module MFunctor.

  Section Defs.

    Variables (A : Type) (B : A → Type).

    Definition FObj X := { a : A & B a → X }.

    Definition Fmap {X Y} (f : X → Y) : FObj X → FObj Y :=
      λ x ∙ existT (λ a ∙ B a → Y) (ρ₁ x) (λ i ∙ f (ρ₂ x i)).

    Definition Feq {X} (eqX : X → X → Prop) : FObj X → FObj X → Prop :=
      λ x y ∙ { H : ρ₁ x = ρ₁ y | ∀ i, eqX (ρ₂ x i) (ρ₂ y (rew H in i)) }.

    Section FeqFacts.

      Context {X} {eqX : X → X → Prop}.

      Let P := λ a ∙ B a → X.

      Lemma eqF : ∀ x y Px Py (H : x = y), (∀ i, Px i = Py (rew H in i)) → existT P x Px = existT P y Py.
      Proof.
        intros x y Px Py <- H. apply eq_sigT_sig_eq. exists eq_refl. simpl.
        extensionality i. apply H.
      Qed.

      Lemma Feq_incl : ∀ (R : X → X → Prop), (∀ x y, R x y → eqX x y) → ∀ x y, Feq R x y → Feq eqX x y.
      Proof.
        intros R R_incl [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert Bx By. destruct eq_xy. intros.
        exists eq_refl. auto.
      Qed.

      Lemma Feq_refl {Refl : Reflexive eqX} : Reflexive (Feq eqX).
      Proof.
        intros [x Bx]. exists eq_refl. intro i; apply Refl.
      Qed.

      Lemma Feq_flip : ∀ x y, Feq eqX y x → Feq (λ a b ∙ eqX b a) x y.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext. now exists eq_refl.
      Qed.

      Lemma Feq_trans_rel : ∀ x y, Feq eqX x y → ∀ z, Feq eqX y z → Feq (λ a c ∙ ∃ b, eqX a b ∧ eqX b c) x z.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext₁ [z Bz] [eq_xz Bext₂]. simpl in *. revert Bext₂ Bext₁. revert Bx₂ Bx₁ Bz.
        destruct eq_xz. intros. exists eq_refl. eauto.
      Qed.

      Lemma Feq_map_sym : ∀ x y (H : Feq eqX x y), (∀ i, eqX (ρ₂ y (rew (ρ₁ H) in i)) (ρ₂ x i)) → Feq eqX y x.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]. simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext sym. simpl in *.
        exists eq_refl. simpl; intros. apply sym.
      Qed.

      Lemma Feq_sym {Sym: Symmetric eqX} : Symmetric (Feq eqX).
      Proof.
        intros x y eq_xy. eapply Feq_map_sym. intro i. apply Sym. apply (ρ₂ eq_xy).
      Qed.

      Lemma Feq_trans {Trans: Transitive eqX} : Transitive (Feq eqX).
      Proof.
        intros [x Bx] [y By] [z Bz] [eq_xy Bext₁] [eq_yz Bext₂]. unfold Feq; simpl in *.
        revert Bext₂. revert eq_yz. revert Bext₁. revert By Bx. destruct eq_xy.
        intros. revert Bext₂. revert Bext₁. revert Bx By Bz. destruct eq_yz.
        intros Bx₁ Bx₂ Bx₃ Bext₁ Bext₂.
        exists eq_refl. intros i. etransitivity; eauto.
      Qed.

      Infix "⁃" := eq_trans (at level 70).
      Notation "p ⁎ ( x )" := (rew p in x) (at level 0, x at next level).

    End FeqFacts.

    Program Definition F : Functor 𝑺𝒆𝒕𝒐𝒊𝒅 𝑺𝒆𝒕𝒐𝒊𝒅 :=
      Functor.make ⦃ F ≔ λ X ∙
                       Setoids.make ⦃ Carrier ≔ FObj X
                                    ; Equiv ≔ Feq (@SEquiv X) ⦄
                   ; map ≔ λ U V ∙ λ f ↦ Setoids.Morphism.make (Fmap f) ⦄.
    Next Obligation.
      constructor.
      - apply Feq_refl.
      - apply Feq_sym.
      - apply Feq_trans.
    Qed.
    Next Obligation.
      destruct x as [x Bx]; destruct y as [y By]; destruct H as [eq_xy Bext]; simpl in *.
      exists eq_xy. intro i; now apply Setoids.cong.
    Qed.
    Next Obligation.
      intros f g eq_fg x y eq_xy.
      destruct x as [x Bx]. destruct y as [y By]. destruct eq_xy as [eq_xy Bext]. simpl in *.
      exists eq_xy. intros i. etransitivity. apply Setoids.cong. apply Bext. apply eq_fg. reflexivity.
    Qed.
    Next Obligation.
      destruct x as [x Bx]; destruct y as [y By]; destruct H as [eq_xy Bext]; simpl in *.
      exists eq_xy. intros i. unfold Fmap in *. simpl in *. now do 2 apply Setoids.cong.
    Qed.

  End Defs.

  Arguments Fmap {_ _ _ _} _ _.
  Arguments Feq  {_ _ _} _ _ _.


End MFunctor.

(*------------------------------------------------------------------------------
  -- ＡＸＩＯＭＡＴＩＺＡＴＩＯＮ  ＯＦ  Ｍ  (ＳＥＴＯＩＤ)
  ----------------------------------------------------------------------------*)

Module Type MSetoid.

  Axiom M : ∀ A, (A → Type) → Type.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Axiom destr : M A B → F (M A B).

    Axiom bisim : M A B → M A B → Prop.
    Notation "_∼_" := bisim (only parsing).
    Infix "∼" := bisim (at level 70).

    Axiom bisim_destr : ∀ {s₁ s₂ : M A B}, s₁ ∼ s₂ → Feq _∼_ (destr s₁) (destr s₂).
    Axiom bisim_intro : ∀ (R : M A B → M A B → Prop)
                          (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)),
                          ∀ {s₁ s₂}, R s₁ s₂ → s₁ ∼ s₂.


  (** ** Corecursor on Stream and computation rules **)
    Axiom corec : ∀ {T}, (T → F T) → T → M A B.
    Axiom destr_corec : ∀ {T} {d : T → F T} {t}, Feq _∼_ (destr (corec d t)) (Fmap (corec d) (d t)).

  End Defs.

  Infix "∼" := bisim (at level 70).

End MSetoid.

(*------------------------------------------------------------------------------
  -- ＡＸＩＯＭＡＴＩＺＡＴＩＯＮ  ＯＦ  Ｍ  (ＴＹＰＥ)
  ----------------------------------------------------------------------------*)

Module Type MType.

  Axiom M : ∀ A, (A → Type) → Type.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Axiom destr : M A B → F (M A B).

    Axiom coind : ∀ (R : M A B → M A B → Prop)
                    (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)),
                    ∀ {s₁ s₂}, R s₁ s₂ → s₁ = s₂.

  (** ** Corecursor on Stream and computation rules **)
    Axiom corec : ∀ {T}, (T → F T) → T → M A B.
    Axiom destr_corec : ∀ {T} {d : T → F T} {t}, Feq eq (destr (corec d t)) (Fmap (corec d) (d t)).

  End Defs.

End MType.

(*------------------------------------------------------------------------------
  -- ＱＵＯＴＩＥＮＴ
  ----------------------------------------------------------------------------*)

Definition comp {A B C} (g : B → C) (f : A → B) : A → C := λ x ∙ g (f x).

Definition compat {E F} (R : E → E → Prop) (f : E → F) :=
  ∀ x y, R x y → f x = f y.

Structure Quotient E R `{Equivalence E R} : Type :=
{ quo :> Type
; class :> E → quo
; quo_comp : ∀ {x y}, R x y → class x = class y
; quo_comp_rev: ∀ {x y}, class x = class y → R x y
; quo_lift: ∀ {F} (f : E → F), compat R f → quo → F
; quo_lift_prop : ∀ {F} (f : E → F) (C : compat R f), comp (quo_lift f C) class = f
; quo_ind : ∀ (P : quo → Prop), (∀ x, P (class x)) → ∀ x, P x }.

Axiom quotient E R : ∀ `{Equivalence E R}, Quotient E R.

Arguments quo {_ _ _} _.
Arguments class {_ _ _} _ _.
Arguments quo_comp {_ _ _ _ _ _} _.
Arguments quo_lift {_ _ _} _ {_} _ _ _.
Arguments quotient _ _ {_}.

Notation "A / R" := (quotient A R).

Lemma quo_surj : ∀ {E R} `{Equivalence E R} {q : Quotient E R}, ∀ (c : q), ∃ e, c = q(e).
Proof.
  intros. pattern c. apply quo_ind.
  eauto.
Qed.

Lemma quo_lift_prop_bis : ∀ {E R} `{Equivalence E R} {q : Quotient E R} {F} {f : E → F} {C : compat R f},
                            ∀ x, quo_lift q f C (q x) = f x.
Proof.
  intros. change (comp (quo_lift q f C) (class q) x = f x). rewrite quo_lift_prop. reflexivity.
Qed.

Module M_Setoid_Type (S : MSetoid) : MType.

  Import MFunctor.

  Notation "_∼" := S.bisim (only parsing).

  Ltac clean_hyps := repeat match goal with H : _ |- _ => clear H end.

  Section Bisim_Equiv.

    Import S.

    Context {A} {B : A → Type}.

    Let bisim := @bisim A B.
    Notation "_∼_" := bisim (only parsing).

    (* _∼_ is an equivalence relation *)
    Lemma bisim_refl : Reflexive _∼_.
    Proof.
      intro x. apply bisim_intro with (R := λ s₁ s₂ ∙ s₁ = s₂); [clean_hyps| auto].
      intros s ? <-. now apply Feq_refl.
    Qed.

    Lemma bisim_sym : Symmetric _∼_.
    Proof.
      intros s₁ s₂ eq_s₁s₂.
      apply bisim_intro with (R := λ s₁ s₂ ∙ s₂ ∼ s₁); [ clean_hyps | auto ].
      intros. apply Feq_flip. apply bisim_destr. auto.
    Qed.

    Lemma bisim_trans : Transitive _∼_.
    Proof.
      intros m₁ m₂ m₃ eq_m₁m₂ eq_m₂m₃.
      apply bisim_intro with (R := λ s₁ s₃ ∙ ∃ s₂, s₁ ∼ s₂ ∧ s₂ ∼ s₃); [clean_hyps; intros | eauto].
      destruct H as (? & ? & ?). apply bisim_destr in H; apply bisim_destr in H0.
      eapply Feq_trans_rel; eauto.
    Qed.

    Global Instance : Equivalence _∼_.
    Proof.
      constructor; repeat intro.
      - apply bisim_refl.
      - now apply bisim_sym.
      - eapply bisim_trans; eauto.
    Qed.

  End Bisim_Equiv.

  Definition M : ∀ A, (A → Type) → Type :=
    λ A B ∙ S.M A B / S.bisim.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Definition fdestr : S.M A B → F (M A B).
    Proof.
      intro m.
      exists (ρ₁ (S.destr m)). intro i. apply class. exact (ρ₂ (S.destr m) i).
    Defined.

    Lemma fdestr_compat : compat S.bisim fdestr.
    Proof.
      intros x y eq_xy. unfold fdestr.
      apply S.bisim_destr in eq_xy.
      destruct (S.destr x) as [rx brx]. destruct (S.destr y) as [ry bry]. simpl in *.
      unfold Feq in eq_xy. simpl in *.
      destruct eq_xy as [eq bisim].
      apply eqF with eq. intro i. apply quo_comp. apply bisim.
    Qed.

    Definition destr : M A B → F (M A B).
    Proof.
      apply (quo_lift _ fdestr fdestr_compat).
    Defined.

    Lemma destr_eq : ∀ x, destr (class _ x) = fdestr x.
    Proof.
      intros. apply quo_lift_prop_bis.
    Qed.

    Section Coind.

      Variables (R : M A B → M A B → Prop)
                (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)).

      Definition R' : S.M A B → S.M A B → Prop.
      Proof.
        intros x y. apply R. apply class. exact x. apply class. exact y.
      Defined.

      Lemma R_destr' : ∀ {s₁ s₂}, R' s₁ s₂ → Feq R' (S.destr s₁) (S.destr s₂).
      Proof.
        intros.
        unfold Feq.
        unfold R' in H. specialize (R_destr _ _ H). unfold Feq in R_destr. simpl in *.
        repeat rewrite destr_eq in R_destr.
        unfold fdestr in *. simpl in *.
        apply R_destr.
      Qed.

      Lemma coind : ∀ {s₁ s₂}, R s₁ s₂ → s₁ = s₂.
      Proof.
        intros.
        generalize (@S.bisim_intro A B); intro.
        assert (R'xy : ∃ x y, s₁ = class _ x ∧ s₂ = class _ y ∧ R' x y). {
          destruct (quo_surj s₁) as [x Hx]. destruct (quo_surj s₂) as [y Hy].
          exists x. exists y. unfold R'. subst. auto.
        }
        destruct R'xy as (x & y & -> & -> & R'xy).
        apply quo_comp.
        apply H0 with R'. intros. now apply R_destr'.
        apply R'xy.
      Qed.

    End Coind.

  (** ** Corecursor on Stream and computation rules **)
    Definition corec : ∀ {T}, (T → F T) → T → M A B.
    Proof.
      intros T T_destr t.
      apply class.
      eapply S.corec; eauto.
    Defined.

    Lemma destr_corec : ∀ {T} {d : T → F T} {t}, Feq eq (destr (corec d t)) (Fmap (corec d) (d t)).
    Proof.
      intros.
      unfold Feq.
      generalize (@S.destr_corec); intro.
      specialize (H _ _ T d t). unfold Feq in H.
      unfold corec. repeat rewrite destr_eq. unfold fdestr. simpl.
      unfold Fmap in H. simpl in *.
      exists (ρ₁ H). intro i. apply quo_comp. apply (ρ₂ H i).
    Qed.

  End Defs.

End M_Setoid_Type.

Module M_Type_Setoid (Import T : MType) : MSetoid.

  Import MFunctor.

  Definition M : ∀ A, (A → Type) → Type := T.M.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Definition destr : M A B → F (M A B) := T.destr.

    Definition bisim : M A B → M A B → Prop := eq.
    Notation "_∼_" := bisim (only parsing).
    Infix "∼" := bisim (at level 70).

    Lemma bisim_destr : ∀ {s₁ s₂ : M A B}, s₁ ∼ s₂ → Feq _∼_ (destr s₁) (destr s₂).
    Proof.
      intros. rewrite H. apply Feq_refl. eauto with typeclass_instances.
    Qed.

    Lemma bisim_intro : ∀ (R : M A B → M A B → Prop)
                          (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)),
                          ∀ {s₁ s₂}, R s₁ s₂ → s₁ ∼ s₂.
    Proof.
      intros. eapply T.coind; eauto.
    Qed.


  (** ** Corecursor on Stream and computation rules **)
    Definition corec : ∀ {T}, (T → F T) → T → M A B := λ T d t ∙ @T.corec _ _ T d t.
    Lemma destr_corec : ∀ {T} {d : T → F T} {t}, Feq _∼_ (destr (corec d t)) (Fmap (corec d) (d t)).
    Proof.
      intros. apply T.destr_corec.
    Qed.

  End Defs.

  Infix "∼" := bisim (at level 70).

End M_Type_Setoid.
