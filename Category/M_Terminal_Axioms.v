Require Import Logic.Eqdep.
Require Import Relations.Relation_Operators.
Require Import Category.Types.
Require Import Category.Setoids.
Require Import Category.Types_Setoids.
Require Import Category.CoAlg.

Require Import Theory.Category.
Require Import Theory.Functor.
Require Import Theory.Product.
Require Import Theory.Isomorphism.
Require Import Theory.InitialTerminal.
Require Import Theory.CoAlgebra.

Generalizable All Variables.

Import EqNotations.
Notation ρ₁ := projT1.
Notation ρ₂ := projT2.

Lemma rew_concat : ∀ {A} {P : A → Type} {a b c : A} (p : a = b) (q : b = c) (i : P a),
                     rew q in rew p in i = rew (eq_trans p q) in i.
Proof.
  intros A P a b c p. destruct p. intro q. destruct q. auto.
Qed.

(*------------------------------------------------------------------------------
  -- Ｍ  ＥＮＤＯＦＵＮＣＴＯＲ
  ----------------------------------------------------------------------------*)

Module MFunctor.

  Section Defs.

    Variables (A : Type) (B : A → Type).

    Definition FObj X := { a : A & B a → X }.

    Definition Fmap {X Y} (f : X → Y) : FObj X → FObj Y :=
      λ x ∙ existT (λ a ∙ B a → Y) (ρ₁ x) (λ i ∙ f (ρ₂ x i)).

    Definition Feq {X} (eqX : X → X → Prop) : FObj X → FObj X → Prop :=
      λ x y ∙ { H : ρ₁ x = ρ₁ y | ∀ i, eqX (ρ₂ x i) (ρ₂ y (rew H in i)) }.

    Section FeqFacts.

      Context {X} {eqX : X → X → Prop}.

      Lemma Feq_incl : ∀ (R : X → X → Prop), (∀ x y, R x y → eqX x y) → ∀ x y, Feq R x y → Feq eqX x y.
      Proof.
        intros R R_incl [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert Bx By. destruct eq_xy. intros.
        exists eq_refl. auto.
      Qed.

      Lemma Feq_refl {Refl : Reflexive eqX} : Reflexive (Feq eqX).
      Proof.
        intros [x Bx]. exists eq_refl. intro i; apply Refl.
      Qed.

      Lemma Feq_flip : ∀ x y, Feq eqX y x → Feq (λ a b ∙ eqX b a) x y.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext. now exists eq_refl.
      Qed.

      Lemma Feq_trans_rel : ∀ x y, Feq eqX x y → ∀ z, Feq eqX y z → Feq (λ a c ∙ ∃ b, eqX a b ∧ eqX b c) x z.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext₁ [z Bz] [eq_xz Bext₂]. simpl in *. revert Bext₂ Bext₁. revert Bx₂ Bx₁ Bz.
        destruct eq_xz. intros. exists eq_refl. eauto.
      Qed.

      Lemma Feq_map_sym : ∀ x y (H : Feq eqX x y), (∀ i, eqX (ρ₂ y (rew (ρ₁ H) in i)) (ρ₂ x i)) → Feq eqX y x.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]. simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext sym. simpl in *.
        exists eq_refl. simpl; intros. apply sym.
      Qed.

      Lemma Feq_sym {Sym: Symmetric eqX} : Symmetric (Feq eqX).
      Proof.
        intros x y eq_xy. eapply Feq_map_sym. intro i. apply Sym. apply (ρ₂ eq_xy).
      Qed.

      Lemma Feq_trans {Trans: Transitive eqX} : Transitive (Feq eqX).
      Proof.
        intros [x Bx] [y By] [z Bz] [eq_xy Bext₁] [eq_yz Bext₂]. unfold Feq; simpl in *.
        revert Bext₂. revert eq_yz. revert Bext₁. revert By Bx. destruct eq_xy.
        intros. revert Bext₂. revert Bext₁. revert Bx By Bz. destruct eq_yz.
        intros Bx₁ Bx₂ Bx₃ Bext₁ Bext₂.
        exists eq_refl. intros i. etransitivity; eauto.
      Qed.

      Infix "⁃" := eq_trans (at level 70).
      Notation "p ⁎ ( x )" := (rew p in x) (at level 0, x at next level).

    End FeqFacts.

    Program Definition F : Functor 𝑺𝒆𝒕𝒐𝒊𝒅 𝑺𝒆𝒕𝒐𝒊𝒅 :=
      Functor.make ⦃ F ≔ λ X ∙
                       Setoids.make ⦃ Carrier ≔ FObj X
                                    ; Equiv ≔ Feq (@SEquiv X) ⦄
                   ; map ≔ λ U V ∙ λ f ↦ Setoids.Morphism.make (Fmap f) ⦄.
    Next Obligation.
      constructor.
      - apply Feq_refl.
      - apply Feq_sym.
      - apply Feq_trans.
    Qed.
    Next Obligation.
      destruct x as [x Bx]; destruct y as [y By]; destruct H as [eq_xy Bext]; simpl in *.
      exists eq_xy. intro i; now apply Setoids.cong.
    Qed.
    Next Obligation.
      intros f g eq_fg x y eq_xy.
      destruct x as [x Bx]. destruct y as [y By]. destruct eq_xy as [eq_xy Bext]. simpl in *.
      exists eq_xy. intros i. etransitivity. apply Setoids.cong. apply Bext. apply eq_fg. reflexivity.
    Qed.
    Next Obligation.
      destruct x as [x Bx]; destruct y as [y By]; destruct H as [eq_xy Bext]; simpl in *.
      exists eq_xy. intros i. unfold Fmap in *. simpl in *. now do 2 apply Setoids.cong.
    Qed.

  End Defs.

  Arguments Fmap {_ _ _ _} _ _.
  Arguments Feq  {_ _ _} _ _ _.

End MFunctor.

(*------------------------------------------------------------------------------
  -- ＡＸＩＯＭＡＴＩＺＡＴＩＯＮ  ＯＦ  Ｍ
  ----------------------------------------------------------------------------*)

Module Type MAxioms.

  Axiom M : ∀ A, (A → Type) → Type.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Axiom destr : M A B → F (M A B).

    Axiom bisim : M A B → M A B → Prop.
    Notation "_∼_" := bisim (only parsing).
    Infix "∼" := bisim (at level 70).

    Axiom bisim_destr : ∀ {s₁ s₂ : M A B}, s₁ ∼ s₂ → Feq _∼_ (destr s₁) (destr s₂).
    Axiom bisim_intro : ∀ (R : M A B → M A B → Prop)
                          (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)),
                          ∀ {s₁ s₂}, R s₁ s₂ → s₁ ∼ s₂.


  (** ** Corecursor on Stream and computation rules **)
    Axiom corec : ∀ {T}, (T → F T) → T → M A B.
    Axiom destr_corec : ∀ {T} {d : T → F T} {t}, Feq _∼_ (destr (corec d t)) (Fmap (corec d) (d t)).

  End Defs.

  Infix "∼" := bisim (at level 70).

End MAxioms.


(*------------------------------------------------------------------------------
  -- Ｍ-ＣＯＡＬＧＥＢＲＡ  ＨＡＳ  Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Type MHasTerminal.

  Parameter ONE : ∀ {A B}, Terminal (𝑪𝒐𝑨𝒍𝒈 (MFunctor.F A B)).
  Notation "'𝟙'" := ONE.

End MHasTerminal.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟸ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Terminal_Axioms (Import MT : MHasTerminal) <: MAxioms.

  Import MFunctor.

  (* Stream Coalgebra *)
  Notation 𝑴 A B := (𝑪𝒐𝑨𝒍𝒈 (F A B)).
  (* Carrier of the coalgebra, i.e a setoid *)
  Notation "⌜ S ⌝" := (CoAlgebra.A _ S).

  (* The final M coalgebra *)
  Notation "'𝐌'" := (⟨⊤⟩ _ 𝟙) (only parsing).
  Notation "'𝐌[' A ; B ]" := (⟨⊤⟩ _ (@MT.ONE A B)).
  (* Unique coalgebra morphism to the final object *)
  Notation "'⇒𝐌'" := (!-⊤ _ 𝟙) (only parsing).
  Notation "[ A ']⇒𝐌'" := (@top _ 𝟙 A).

  (** ** Stream type and destructors **)
  Definition M : ∀ A, (A → Type) → Type := λ A B ∙ ⌜𝐌[A;B]⌝.

  Section Bisim.
    Context {A} {B : A → Type}.

    Arguments F {_ _}.
    Arguments FObj {_ _} _.

    Definition destr' : M A B ⇒ F ⌜𝐌[A;B]⌝ := α(𝐌).
    Definition destr : M A B → FObj (M A B) := destr'.

    Definition bisim : M A B → M A B → Prop := @SEquiv ⌜𝐌[A;B]⌝.
    Notation "_∼_" := bisim (only parsing).
    Infix "∼" := bisim (at level 70).

    Lemma bisim_destr : ∀ {s₁ s₂ : M A B}, s₁ ∼ s₂ → Feq _∼_ (destr s₁) (destr s₂).
    Proof.
      intros s₁ s₂ eq_s₁s₂. change (SEquiv (destr' s₁) (destr' s₂)).
      now rewrite eq_s₁s₂.
    Qed.

    Section Bisim_Intro.
      Variables (R : M A B → M A B → Prop)
                (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)).

      Definition bisim_R := clos_refl_sym_trans _ (union _ _∼_ R).

      Notation "_≃_" := bisim_R (only parsing).
      Infix "≃" := bisim_R (at level 70).

      Tactic Notation "is-∼" := apply rst_step; left.
      Tactic Notation "is-R" := apply rst_step; right.

      Instance bisim_R_equiv : Equivalence _≃_.
      Proof.
        constructor.
        - repeat intro. apply rst_refl.
        - repeat intro. now apply rst_sym.
        - repeat intro. eapply rst_trans; eauto.
      Qed.

      Program Definition NSetoid : ‵ 𝑺𝒆𝒕𝒐𝒊𝒅 ′ :=
        Setoids.make ⦃ Carrier ≔ M A B ; Equiv ≔ _≃_ ⦄.
      Notation "'⌜𝐍⌝'" := NSetoid (only parsing).

      Program Definition 𝐍_destr : ‵ ⌜𝐍⌝ ⇒ F ⌜𝐍⌝ ′ := Setoids.Morphism.make destr.
      Next Obligation.
        induction H.
        - destruct H.
          + exists (ρ₁ (bisim_destr H)). intro i. is-∼. apply (ρ₂ (bisim_destr H)).
          + exists (ρ₁ (R_destr _ _ H)). intro i. is-R. apply (ρ₂ (R_destr _ _ H)).
        - apply Feq_refl. eauto with typeclass_instances.
        - apply Feq_sym; eauto with typeclass_instances.
        - eapply Feq_trans; eauto with typeclass_instances.
      Qed.

      Definition 𝐍 := CoAlgebra.make ⦃ A ≔ ⌜𝐍⌝ ; α ≔ 𝐍_destr ⦄.

      Program Definition 𝐢𝐝 : ‵ 𝐌[A;B] ⇒ 𝐍 ′ := CoAlgebra.make ⦃ τ ≔ Setoids.Morphism.make (λ x ∙ x) ⦄.
      Next Obligation.
        now is-∼.
      Qed.
      Next Obligation.
        assert (SEquiv (𝐍_destr x) (𝐍_destr y)).
        apply Setoids.cong. is-∼. apply H.
        simpl in H0. eapply Feq_trans. eauto with typeclass_instances.
        apply H0. clear H0.
        unfold Fmap. unfold Feq. simpl. exists eq_refl. intro i. now is-∼.
      Qed.

      (* Unique coalgebra morphism from 𝐓 to 𝐒 *)
      Definition 𝐭𝐨𝐩 : 𝐍 ⇒ 𝐌[A;B] := ⇒𝐌.

      (* By finality 𝐭𝐨𝐩 is the identity coalgebra morphism *)
      Lemma 𝐭𝐨𝐩_is_id : id ≈ 𝐭𝐨𝐩 ∘ 𝐢𝐝.
      Proof.
        etransitivity. apply top_unique.
        symmetry. apply top_unique.
      Qed.
      Notation "𝐭𝐨𝐩≈id" := (𝐭𝐨𝐩_is_id).

      Lemma bisim_intro : ∀ x y, R x y → x ∼ y.
      Proof.
        intros x y Rxy.
        etransitivity;[|symmetry; etransitivity]; [now apply 𝐭𝐨𝐩≈id..| ].
        apply (Setoids.cong (τ 𝐭𝐨𝐩)). symmetry; now is-R.
      Qed.

    End Bisim_Intro.

    (** ** Corecursor on Stream and computation rules **)
    Section Corecursor.

      Context {T : Type}.
      Variable (d : T → @FObj A B T).

      Notation "⌜𝐎⌝" := (𝑬𝑸(T)).

      Program Definition 𝐎_destr : ‵ ⌜𝐎⌝ ⇒ F ⌜𝐎⌝ ′ := Setoids.Morphism.make d.
      Next Obligation.
        now apply Feq_refl.
      Qed.

      Definition 𝐎 := CoAlgebra.make ⦃ A ≔ ⌜𝐎⌝ ; α ≔ 𝐎_destr ⦄.


      Definition corec : T → M A B := τ [𝐎]⇒𝐌.

    End Corecursor.

    Lemma destr_corec : ∀ {T} {d : T → FObj T} {t}, Feq _∼_ (destr (corec d t)) (Fmap (corec d) (d t)).
    Proof.
      intros T d t.
      eapply Feq_trans. eauto with typeclass_instances.
      apply (τ_commutes [𝐎 d]⇒𝐌). reflexivity.
      apply Feq_refl. eauto with typeclass_instances.
    Qed.

  End Bisim.

End Terminal_Axioms.



(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟹ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)
Module Axioms_Terminal (Import Ax : MAxioms) <: MHasTerminal.

  Import MFunctor.

  (* Stream Coalgebra *)
  Notation 𝑴 A B := (𝑪𝒐𝑨𝒍𝒈 (F A B)).
  (* Carrier of the coalgebra, i.e a setoid *)
  Notation "⌜ S ⌝" := (CoAlgebra.A _ S).

  (* Notation "_∼_" := bisim (only parsing). *)
  (* Notation "_∼[ A ]_" := (@bisim A) (only parsing). *)

  Ltac clean_hyps := repeat match goal with H : _ |- _ => clear H end.

  Section Bisim_Equiv.

    Context {A} {B : A → Type}.

    Let bisim := @bisim A B.

    Notation "_∼_" := bisim (only parsing).

    (* _∼_ is an equivalence relation *)
    Lemma bisim_refl : Reflexive _∼_.
    Proof.
      intro x. apply bisim_intro with (R := λ s₁ s₂ ∙ s₁ = s₂); [clean_hyps| auto].
      intros s ? <-. now apply Feq_refl.
    Qed.

    Lemma bisim_sym : Symmetric _∼_.
    Proof.
      intros s₁ s₂ eq_s₁s₂.
      apply bisim_intro with (R := λ s₁ s₂ ∙ s₂ ∼ s₁); [ clean_hyps | auto ].
      intros. apply Feq_flip. apply bisim_destr. auto.
    Qed.

  Lemma bisim_trans : Transitive _∼_.
  Proof.
    intros m₁ m₂ m₃ eq_m₁m₂ eq_m₂m₃.
    apply bisim_intro with (R := λ s₁ s₃ ∙ ∃ s₂, s₁ ∼ s₂ ∧ s₂ ∼ s₃); [clean_hyps; intros | eauto].
    destruct H as (? & ? & ?). apply bisim_destr in H; apply bisim_destr in H0.
    eapply Feq_trans_rel; eauto.
  Qed.

  Global Instance bisim_equiv : Equivalence _∼_.
  Proof.
    constructor.
    - apply bisim_refl.
    - apply bisim_sym.
    - apply bisim_trans.
  Qed.

  End Bisim_Equiv.

  Section CoAlgebraDefinition.

    (* We give M A the structure of a 𝑴 coalgebra *)

    Context {A : Type} {B : A → Type}.

    Let F := MFunctor.F A B.

    Notation "_∼_" := bisim (only parsing).

    (* Carrier of the coalgebra *)
    Program Definition MSetoid := Setoids.make ⦃ Carrier ≔ M A B ; Equiv ≔ _∼_ ⦄.
    Notation "'⌜𝐌⌝'" := MSetoid.

    (* Destructor *)
    Program Definition 𝐌_destr : ‵ ⌜𝐌⌝ ⇒ F ⌜𝐌⌝ ′ := Setoids.Morphism.make destr.
    Next Obligation.
      now apply bisim_destr.
    Defined.

    Program Definition 𝐌 : ‵ 𝑴 A B ′ := CoAlgebra.make ⦃ A ≔ ⌜𝐌⌝ ; α ≔ 𝐌_destr ⦄.

    Variable (𝐍 : 𝑴 A B).

    Definition 𝐍_destr : ⌜𝐍⌝ ⇒ F ⌜𝐍⌝ := α(𝐍).

    (* There is a coalgebra morphism from any coalgebra 𝐓 to 𝐒 *)
    Let Corec := corec 𝐍_destr.

    Lemma bisim_eq : ∀ (m₁ m₂ : M A B), m₁ = m₂ → m₁ ∼ m₂.
    Proof.
      intros m₁ m₂ ->; reflexivity.
    Qed.

    Program Definition 𝐭𝐨𝐩 : ‵ 𝐍 ⇒ 𝐌 ′ := CoAlgebra.make ⦃ τ ≔ Setoids.Morphism.make (corec 𝐍_destr) ⦄.
    Next Obligation.
      apply bisim_intro with (R := λ s₁ s₂ ∙ ∃ x y, s₁ ∼ Corec x ∧ Corec y ∼ s₂ ∧ SEquiv x y); [clean_hyps|].
      - intros m₁ m₂ (x & y & eq₁ & eq₂ & eq₃).
        assert (F₁ : Feq bisim (destr m₁) (Fmap Corec (𝐍_destr x))). {
          eapply Feq_trans;[eauto with typeclass_instances|..].
          apply (bisim_destr eq₁).
          apply destr_corec.
        }
        assert (F₂ : Feq bisim (Fmap Corec (𝐍_destr y)) (destr m₂)). {
          eapply Feq_trans;[eauto with typeclass_instances|..].
          apply Feq_sym;[eauto with typeclass_instances|].
          apply destr_corec.
          apply (bisim_destr eq₂).
        }
        assert (E : Feq SEquiv (𝐍_destr x) (𝐍_destr y)). {
          apply (Setoids.cong 𝐍_destr eq₃).
        }
        unfold Feq. exists (eq_trans (ρ₁ F₁) (eq_trans (ρ₁ E) (ρ₁ F₂))).
        intros i. exists (ρ₂ (𝐍_destr x) (rew (ρ₁ F₁) in i)).
                  exists (ρ₂ (𝐍_destr y) (rew (eq_trans (ρ₁ F₁) (ρ₁ E)) in i)).
                  repeat split.
        + apply (ρ₂ F₁ i).
        + rewrite <- (rew_concat (ρ₁ F₁) (eq_trans (ρ₁ E) (ρ₁ F₂)) i).
          rewrite <- (rew_concat (ρ₁ E) (ρ₁ F₂) _).
          rewrite (rew_concat  (ρ₁ F₁) (ρ₁ E) _).
          apply (ρ₂ F₂ (rew (eq_trans (ρ₁ F₁) (ρ₁ E)) in i)).
        + rewrite <- (rew_concat (ρ₁ F₁) (ρ₁ E) _).
          apply (ρ₂ E (rew (ρ₁ F₁) in i)).
      - exists x. exists y. now repeat split.
    Qed.
    Next Obligation.
      eapply Feq_trans;[eauto with typeclass_instances|..].
      apply destr_corec.
      apply Feq_sym;[eauto with typeclass_instances|].
      apply Feq_sym;[eauto with typeclass_instances|].
      unfold Feq. simpl in *.
      set (Setoids.cong (α(𝐍)) H).
      clearbody s. unfold SEquiv in s. simpl in s. unfold Feq in s.
      exists (ρ₁ s). intros i. etransitivity. eapply 𝐭𝐨𝐩_obligation_1.
      apply (ρ₂ s i). reflexivity.
    Qed.

  End CoAlgebraDefinition.

  (* 𝐒 is a terminal object *)
  Program Definition ONE {A B} : Terminal (𝑴 A B) :=
    Terminal.make ⦃ one ≔ @𝐌 A B ; top ≔ 𝐭𝐨𝐩 ⦄.
  Next Obligation.
    rename A0 into N.
    etransitivity. apply (Setoids.cong (τ f) H).
    clear H x. rename y into x.
    set (Corec := corec (𝐍_destr N)).
    apply bisim_intro with (R := λ m₁ m₂ ∙ ∃ x, m₁ ∼ τ f x ∧  Corec x ∼ m₂); [clean_hyps..|].
    - intros m₁ m₂ (x & eq_m₁ & eq_m₂). unfold Feq.
      assert (F₁ : Feq bisim (destr m₁) (Fmap (τ f) (α(N) x))). {
        eapply Feq_trans;[eauto with typeclass_instances|..].
        apply (bisim_destr eq_m₁).
        apply (τ_commutes f). reflexivity.
      }
      assert (F₂ : Feq bisim (Fmap (τ (𝐭𝐨𝐩 N)) (α(N) x)) (destr m₂)). {
        eapply Feq_trans;[eauto with typeclass_instances|..].
        eapply Feq_sym; [eauto with typeclass_instances|].
        apply (τ_commutes (𝐭𝐨𝐩 N)). reflexivity.
        apply (bisim_destr eq_m₂).
      }
      exists (eq_trans (ρ₁ F₁) (ρ₁ F₂)).
      intros i.
      exists (ρ₂ (α(N) x) (rew (ρ₁ F₁) in i)).
      split.
      apply (ρ₂ F₁ i).
      rewrite <- rew_concat.
      apply (ρ₂ F₂).
    - exists x. now repeat split.
  Qed.

End Axioms_Terminal.