(* Require Import Logic.Eqdep. *)
Require Import Relations.Relation_Operators.
Require Import Category.Types.
Require Import Category.Setoids.
Require Import Category.Types_Setoids.
 Require Import Category.CoAlg. 

Require Import Theory.Category.
Require Import Theory.Functor.
Require Import Theory.Product.
Require Import Theory.Isomorphism.
Require Import Theory.InitialTerminal.
Require Import Theory.CoAlgebra. 

Require Import FunctionalExtensionality.

Generalizable All Variables.

Import EqNotations.
Lemma rew_concat : ∀ {A} {P : A → Type} {a b c : A} (p : a = b) (q : b = c) (i : P a),
                     rew q in rew p in i = rew (eq_trans p q) in i.
Proof.
  intros A P a b c p. destruct p. intro q. destruct q. auto.
Qed.

(*------------------------------------------------------------------------------
  -- Ｍ  ＥＮＤＯＦＵＮＣＴＯＲ
  ----------------------------------------------------------------------------*)

Module MFunctor.

  Local Notation ρ₁ := projT1.
  Local Notation ρ₂ := projT2.

  Section Defs.

    Variables (A : Type) (B : A → Type).

    Definition FObj X := { a : A & B a → X }.

    Definition Fmap {X Y} (f : X → Y) : FObj X → FObj Y :=
      λ x ∙ existT (λ a ∙ B a → Y) (ρ₁ x) (λ i ∙ f (ρ₂ x i)).

    Definition Feq {X} (eqX : X → X → Type) : FObj X → FObj X → Type :=
      λ x y ∙ { H : ρ₁ x = ρ₁ y & ∀ i, eqX (ρ₂ x i) (ρ₂ y (rew H in i)) }.

    Section FeqFacts.

      Context {X} {eqX : X → X → Type}.

      Lemma Feq_incl : ∀ (R : X → X → Type), (∀ x y, R x y → eqX x y) → ∀ x y, Feq R x y → Feq eqX x y.
      Proof.
        intros R R_incl [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert Bx By. destruct eq_xy. intros.
        exists eq_refl. auto.
      Qed.

      Lemma Feq_refl {Refl : forall x, eqX x x} : forall y, (Feq eqX) y y.
      Proof.
        intros [x Bx]. exists eq_refl. intro i; apply Refl.
      Qed.

      Lemma Feq_flip : ∀ x y, Feq eqX y x → Feq (λ a b ∙ eqX b a) x y.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext. now exists eq_refl.
      Qed.
(*
      Lemma Feq_trans_rel : ∀ x y, Feq eqX x y → ∀ z, Feq eqX y z → Feq (λ a c ∙ ∃ b, eqX a b ∧ eqX b c) x z.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]; unfold Feq; simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext₁ [z Bz] [eq_xz Bext₂]. simpl in *. revert Bext₂ Bext₁. revert Bx₂ Bx₁ Bz.
        destruct eq_xz. intros. exists eq_refl. eauto.
      Qed.
*)
      Lemma Feq_map_sym : ∀ x y (H : Feq eqX x y), (∀ i, eqX (ρ₂ y (rew (ρ₁ H) in i)) (ρ₂ x i)) → Feq eqX y x.
      Proof.
        intros [x Bx] [y By] [eq_xy Bext]. simpl in *.
        revert Bext. revert By Bx. destruct eq_xy.
        intros Bx₁ Bx₂ Bext sym. simpl in *.
        exists eq_refl. simpl; intros. apply sym.
      Qed.

      Lemma Feq_sym {Sym: forall x y, eqX x y -> eqX y x} : forall x y, (Feq eqX) x y -> Feq eqX y x.
      Proof.
        intros x y eq_xy. eapply Feq_map_sym. intro i. apply Sym. apply (ρ₂ eq_xy).
      Qed.

(*
      Lemma Feq_trans {Trans: Transitive eqX} : Transitive (Feq eqX).
      Proof.
        intros [x Bx] [y By] [z Bz] [eq_xy Bext₁] [eq_yz Bext₂]. unfold Feq; simpl in *.
        revert Bext₂. revert eq_yz. revert Bext₁. revert By Bx. destruct eq_xy.
        intros. revert Bext₂. revert Bext₁. revert Bx By Bz. destruct eq_yz.
        intros Bx₁ Bx₂ Bx₃ Bext₁ Bext₂.
        exists eq_refl. intros i. etransitivity; eauto.
      Qed.
*)
      Infix "⁃" := eq_trans (at level 70).
      Notation "p ⁎ ( x )" := (rew p in x) (at level 0, x at next level).

    End FeqFacts.

    Program Definition F : Functor 𝑻𝒚𝒑𝒆 𝑻𝒚𝒑𝒆 :=
      Functor.make ⦃ F ≔ FObj
                   ; map ≔ λ U V ∙ λ f ↦ Fmap f ⦄.
    Next Obligation.
      intros f g eq_fg x. f_equal. extensionality y. apply eq_fg.
    Qed.
    Next Obligation.
      destruct x. unfold Fmap. simpl in *. unfold Types.id. f_equal.
    Qed.

  End Defs.

  Arguments Fmap {_ _ _ _} _ _.
  Arguments Feq  {_ _ _} _ _ _.

End MFunctor.

(*------------------------------------------------------------------------------
  -- ＡＸＩＯＭＡＴＩＺＡＴＩＯＮ  ＯＦ  Ｍ
  ----------------------------------------------------------------------------*)

Module Type MAxioms.

  Axiom M : ∀ A, (A → Type) → Type.

  Section Defs.

    Context {A : Type} {B : A → Type}.

    Import MFunctor.

    Let F := MFunctor.FObj A B.

    Axiom destr : M A B → F (M A B).

    Axiom coind : ∀ (R : M A B → M A B → Type)
                          (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)),
                          ∀ {s₁ s₂}, R s₁ s₂ → s₁ = s₂.


  (** ** Corecursor on Stream and computation rules **)
    Axiom corec : ∀ {T}, (T → F T) → T → M A B.
    Axiom destr_corec : ∀ {T} {d : T → F T} {t}, Feq eq (destr (corec d t)) (Fmap (corec d) (d t)).

  End Defs.

  Infix "∼" := eq (at level 70).

End MAxioms.


(*------------------------------------------------------------------------------
  -- Ｍ-ＣＯＡＬＧＥＢＲＡ  ＨＡＳ  Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Type MHasTerminal.

  Parameter ONE : ∀ {A B}, Terminal (𝑪𝒐𝑨𝒍𝒈 (MFunctor.F A B)).
  Notation "'𝟙'" := ONE.

End MHasTerminal.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟸ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Terminal_Axioms (Import MT : MHasTerminal) <: MAxioms.

  Import MFunctor.

  (* Stream Coalgebra *)
  Notation 𝑴 A B := (𝑪𝒐𝑨𝒍𝒈 (F A B)).
  (* Carrier of the coalgebra, i.e a type *)
  Notation "⌜ S ⌝" := (CoAlgebra.A _ S).

  (* The final M coalgebra *)
  Notation "'𝐌'" := (⟨⊤⟩ _ 𝟙) (only parsing).
  Notation "'𝐌[' A ; B ]" := (⟨⊤⟩ _ (@MT.ONE A B)).
  (* Unique coalgebra morphism to the final object *)
  Notation "'⇒𝐌'" := (!-⊤ _ 𝟙) (only parsing).
  Notation "[ A ']⇒𝐌'" := (@top _ 𝟙 A).

  Infix "∼" := eq (at level 70).

  (** ** Stream type and destructors **)
  Definition M : ∀ A, (A → Type) → Type := λ A B ∙ ⌜𝐌[A;B]⌝.

  Section Bisim_Intro.

    Context {A} {B : A → Type}.

    Arguments F {_ _}.
    Arguments FObj {_ _} _.

    Definition destr' : M A B ⇒ F ⌜𝐌[A;B]⌝ := α(𝐌).
    Definition destr : M A B → FObj (M A B) := destr'.

    Variables (R : M A B → M A B → Type)
              (R_destr : ∀ {s₁ s₂}, R s₁ s₂ → Feq R (destr s₁) (destr s₂)).

    Definition Rel {A} (R : A → A → Type) := { X : _ & R (fst X) (snd X) }. 
    Definition ρ₁ {A} {R} (r : Rel R) : A := fst (projT1 r).
    Definition ρ₂ {A} {R} (r : Rel R) : A := snd (projT1 r).

    Definition 𝓡 := Rel R.

    Definition 𝓡_root₁ : 𝓡 → A := λ x ∙ projT1 (destr (ρ₁ x)).
    Definition 𝓡_root₂ : 𝓡 → A := λ x ∙ projT1 (destr (ρ₂ x)).
    Definition 𝓡_root_eq : ∀ r, 𝓡_root₁ r = 𝓡_root₂ r := λ r ∙ projT1 (R_destr _ _ (projT2 r)).

    Definition 𝓡_destr : 𝓡 → @FObj A B (𝓡).
    Proof.
      intros r. exists (𝓡_root₁ r). intro i.
      exists ((projT2 (destr (ρ₁ r)) i) , (projT2 (destr (ρ₂ r)) (rew (𝓡_root_eq r) in i))).
      simpl. exact (projT2 (R_destr _ _ (projT2 r)) i).
    Defined.

    Program Definition 𝐑 : ‵ 𝑴 A B ′ := CoAlgebra.make ⦃ A ≔ 𝓡 ; α ≔ 𝓡_destr ⦄.

    Lemma η : ∀ {A} {B : A → Type} (x : { a : A & B a → M A B }),
                x = existT (λ a ∙ B a → M A B) (projT1 x) (λ i ∙ projT2 x i).
    Proof.
      clear A B R R_destr. intros A B x. destruct x. simpl. f_equal.
    Qed.

    (** ρ₁ is a morphism of coalgebra **)
    Program Definition θ₁ : ‵ 𝐑 ⇒ 𝐌[A;B] ′ := CoAlgebra.make ⦃ τ ≔ ρ₁ ⦄.
    Next Obligation.
      destruct x as ((m₁ , m₂) , r).
      apply η.
    Qed.

    Lemma helper : ∀ (x : { a : A & B a → M A B }) (y : A) (H : y = projT1 x),
                  x = existT _ y (λ i ∙ projT2 x (rew H in i)).
    Proof.
      intros (x , Bx) y H.  simpl in *. revert Bx. destruct H.
      intros. simpl. apply η.
    Qed.

    Program Definition θ₂ : ‵ 𝐑 ⇒ 𝐌[A;B] ′ := CoAlgebra.make ⦃ τ ≔ ρ₂ ⦄.
    Next Obligation.
      apply helper.
    Qed.

    Lemma coind : ∀ s₁ s₂, R s₁ s₂ → s₁ = s₂.
    Proof.
      intros. set (r := existT (λ X ∙ R (fst X) (snd X)) (s₁, s₂) X).
      generalize (@top_unique _ ONE _ θ₁); intro.
      generalize (@top_unique _ ONE _ θ₂); intro.
      assert (θ₁ ≈ θ₂). etransitivity. apply H. symmetry. apply H0.
      simpl in *. specialize (H1 r). unfold r in H1. unfold ρ₁,ρ₂ in H1. simpl in H1. apply H1.
    Qed.

  End Bisim_Intro.

  (** ** Corecursor on Stream and computation rules **)
  Section Corecursor.

    Context {A : Type} {B : A → Type} {T : Type}.
    Variable (d : T → @FObj A B T).

    Program Definition 𝐓 : ‵ 𝑴 A B ′ := CoAlgebra.make ⦃ A ≔ T ; α ≔ d ⦄.

    Definition corec : T → M A B := τ [𝐓]⇒𝐌.

  End Corecursor.

  Lemma helper₂ : ∀ {A} {B : A → Type}, ∀ (x y : {a : A & B a → M A B}), x = y → Feq eq x y.
    intros ? ? x y ->. apply Feq_refl. eauto with typeclass_instances.
  Qed.

  Lemma destr_corec : ∀ {A} {B : A → Type} {T} {d : T → @FObj A B T} {t},
                        Feq eq (destr (corec d t)) (Fmap (corec d) (d t)).
  Proof.
    intros.
    apply helper₂. apply (τ_commutes ([𝐓 d]⇒𝐌)).
  Qed.

End Terminal_Axioms.



(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟹ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)
Module Axioms_Terminal (Import Ax : MAxioms) <: MHasTerminal.

  Import MFunctor.

  (* Stream Coalgebra *)
  Notation 𝑴 A B := (𝑪𝒐𝑨𝒍𝒈 (F A B)).

  Section CoAlgebraDefinition.

    (* We give M A the structure of a 𝑴 coalgebra *)

    Context {A : Type} {B : A → Type}.

    Let F := MFunctor.F A B.

    Program Definition 𝐌 : ‵ 𝑴 A B ′ := CoAlgebra.make ⦃ A ≔ (M A B) ; α ≔ destr ⦄.

    Variable (𝐍 : 𝑴 A B).


    (* There is a coalgebra morphism from any coalgebra 𝐓 to 𝐒 *)
    Let Corec := corec (α(𝐍)).

    Lemma η : ∀ {A} {B : A → Type} (x : { a : A & B a → M A B }),
                x = existT (λ a ∙ B a → M A B) (projT1 x) (λ i ∙ projT2 x i).
    Proof.
      intros ? ? x. destruct x. simpl. f_equal.
    Qed.

    Lemma helper : ∀ (x y : {a : A & B a → M A B}),
                     { H : projT1 x = projT1 y & ∀ i, projT2 x i = projT2 y (rew H in i) } → x = y.
    Proof.
      intros (x, Bx) (y, By) (eqr, ext). simpl in *.
      revert ext. revert Bx By. destruct eqr. intros. simpl in *.
      assert (Bx = By). extensionality u. apply ext. rewrite H. reflexivity.
    Qed.

    Program Definition 𝐭𝐨𝐩 : ‵ 𝐍 ⇒ 𝐌 ′ := CoAlgebra.make ⦃ τ ≔ corec (α(𝐍)) ⦄.
    Next Obligation.
      apply helper. apply destr_corec.
    Qed.

    Lemma helper₂ : ∀ (x y : {a : A & B a → M A B}), x = y → Feq eq x y.
      intros x y ->. apply Feq_refl. eauto with typeclass_instances.
    Qed.

  End CoAlgebraDefinition.

  (* 𝐒 is a terminal object *)
  Program Definition ONE {A B} : Terminal (𝑴 A B) :=
    Terminal.make ⦃ one ≔ @𝐌 A B ; top ≔ 𝐭𝐨𝐩 ⦄.
  Next Obligation.
    rename A0 into T. 
    apply coind with (R := λ s₁ s₂ ∙ sigT (fun x => s₁ = τ f x ∧ corec (α(T)) x = s₂)). clear x.
    intros s₁ s₂ (x & -> & <-).
    set (Corec := corec (α(T))).
    assert (F₁ : Feq eq (destr (τ f x)) (Fmap (τ f) (α(T) x))). {
        apply helper₂. apply (τ_commutes f x).
    }
    assert (F₂ : Feq eq (Fmap (τ (𝐭𝐨𝐩 T)) (α(T) x)) (destr (Corec x))). {
      eapply Feq_sym. eauto with typeclass_instances.
      apply helper₂. apply (τ_commutes (𝐭𝐨𝐩 T) x).
    }
    exists (eq_trans (projT1 F₁) (projT1 F₂)).
    intros i.
    exists (projT2 (α(T) x) (rew (projT1 F₁) in i)).
    split.
    apply (projT2 F₁ i).
    rewrite <- rew_concat.
    apply (projT2 F₂).
    - exists x. now repeat split.
  Qed.

End Axioms_Terminal.