Require Import Category.Types.
Require Import Category.CoAlg.

Require Import Theory.Category.
Require Import Theory.Functor.
Require Import Theory.Product.
Require Import Theory.Isomorphism.
Require Import Theory.InitialTerminal.
Require Import Theory.CoAlgebra.

(*------------------------------------------------------------------------------
  -- ＡＸＩＯＭＡＴＩＺＡＴＩＯＮ  ＯＦ  ＳＴＲＥＡＭＳ
  ----------------------------------------------------------------------------*)

Module Type StreamAxioms.

  (** ** Stream type and destructors **)
  Axiom Stream : Type → Type.
  Axiom head : ∀ {A}, Stream A → A.
  Axiom tail : ∀ {A}, Stream A → Stream A.

  (** ** coinduction principle **)
  Axiom coind : ∀ {A} (R : Stream A → Stream A → Prop)
                  (R_head : ∀ s₁ s₂, R s₁ s₂ → head s₁ = head s₂)
                  (R_tail : ∀ s₁ s₂, R s₁ s₂ → R (tail s₁) (tail s₂)),
                  ∀ s₁ s₂, R s₁ s₂ → s₁ = s₂.

  (** ** Corecursor on Stream and computation rules **)
  Axiom corec : ∀ {A T}, (T → A) → (T → T) → T → Stream A.
  Axiom head_corec : ∀ {A T} {hd : T → A} {tl : T → T} {t}, head (corec hd tl t) = hd t.
  Axiom tail_corec : ∀ {A T} {hd : T → A} {tl : T → T} {t}, tail (corec hd tl t) = corec hd tl (tl t).

End StreamAxioms.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＥＮＤＯＦＵＮＣＴＯＲ
  ----------------------------------------------------------------------------*)

Module StreamFunctor.

  Program Definition F (A : 𝑻𝒚𝒑𝒆) : Functor 𝑻𝒚𝒑𝒆 𝑻𝒚𝒑𝒆 :=
    Functor.make ⦃ F ≔ λ X ∙ A × X
                 ; map ≔ λ A B ∙ λ f ↦ ⟨ π₁ , f ∘ π₂ ⟩  ⦄.
  Next Obligation.
    intros f g eq_fg [a b]. congruence.
  Qed.

End StreamFunctor.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ  ＨＡＳ  Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Type StreamHasTerminal.

  Parameter S : ∀ {A : 𝑻𝒚𝒑𝒆}, Terminal (𝑪𝒐𝑨𝒍𝒈 (StreamFunctor.F A)).

End StreamHasTerminal.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟹ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)
Module Axioms_Terminal (Import Ax : StreamAxioms) : StreamHasTerminal.

  Import StreamFunctor.

  (* Stream Coalgebra *)
  Notation 𝑺𝒕𝒓𝒆𝒂𝒎 A := (𝑪𝒐𝑨𝒍𝒈 (F A)).
  (* Carrier of the coalgebra, i.e a setoid *)
  Notation "⌜ S ⌝" := (CoAlgebra.A _ S).

  Ltac clean_hyps := repeat match goal with H : _ |- _ => clear H end.

  Section CoAlgebraDefinition.

    (* We give Stream A the structure of a 𝑺𝒕𝒓𝒆𝒂𝐦 coalgebra *)

    Variable (A : Type).

    (* Carrier of the coalgebra *)
    Definition S_ := Stream A.
    Notation "'⌜𝐒⌝'" := S_.

    (* Destructors *)
    Definition 𝐒_head : ‵ ⌜𝐒⌝ ⇒ A ′ := head.

    Definition 𝐒_tail : ‵ ⌜𝐒⌝ ⇒ ⌜𝐒⌝ ′ := tail.

    Program Definition 𝐒 : ‵ 𝑺𝒕𝒓𝒆𝒂𝒎 A ′ := CoAlgebra.make ⦃ A ≔ ⌜𝐒⌝ ; α ≔ ⟨ 𝐒_head , 𝐒_tail ⟩ ⦄.

    Variable (𝐓 : 𝑺𝒕𝒓𝒆𝒂𝒎 A).

    Definition 𝐓_head : ⌜𝐓⌝ ⇒ A := π₁ ∘ α(𝐓).
    Definition 𝐓_tail : ⌜𝐓⌝ ⇒ ⌜𝐓⌝ := π₂ ∘ α(𝐓).

    (* There is a coalgebra morphism from any coalgebra 𝐓 to 𝐒 *)

    Program Definition 𝐭𝐨𝐩 : ‵ 𝐓 ⇒ 𝐒 ′ := CoAlgebra.make ⦃ τ ≔ corec 𝐓_head 𝐓_tail ⦄.
    Next Obligation.
      f_equal.
      - unfold 𝐒_head. rewrite head_corec. reflexivity.
      - unfold 𝐒_tail. rewrite tail_corec. reflexivity.
    Qed.
  End CoAlgebraDefinition.

  (* 𝐒 is a terminal object *)
  Program Definition S {A} : Terminal (𝑺𝒕𝒓𝒆𝒂𝒎 A) :=
    Terminal.make ⦃ one ≔ 𝐒 A ; top ≔ 𝐭𝐨𝐩 A ⦄.
  Next Obligation.
    rename A0 into T.
    apply coind with (R := λ s₁ s₂ ∙ ∃ x, s₁ = τ f x ∧ s₂ = corec (𝐓_head A T) (𝐓_tail A T) x); [clean_hyps..|].
    - intros s₁ s₂ (x & -> & ->).
      rewrite head_corec. generalize (τ_commutes f); intro. simpl in H. specialize (H x). injection H. intros.
      auto.
    - intros s₁ s₂ (x & -> & ->). exists (𝐓_tail _ _ x). repeat split.
      + injection (τ_commutes f x); auto.
      + now rewrite tail_corec.
    - exists x. auto.
  Qed.
End Axioms_Terminal.

(*------------------------------------------------------------------------------
  -- ＳＴＲＥＡＭ  ＡＸＩＯＭＳ  ⟸ ＳＴＲＥＡＭ-ＣＯＡＬＧＥＢＲＡ ＨＡＳ
                                               Ａ  ＴＥＲＭＩＮＡＬ  ＯＢＪＥＣＴ
  ----------------------------------------------------------------------------*)

Module Terminal_Axioms (Import MT : StreamHasTerminal) : StreamAxioms.

  Import StreamFunctor.

  (* Stream Coalgebra *)
  Notation 𝑺𝒕𝒓𝒆𝒂𝒎 A := (𝑪𝒐𝑨𝒍𝒈 (F A)).
  (* Carrier of the coalgebra, i.e a setoid *)
  Notation "⌜ S ⌝" := (CoAlgebra.A _ S).

  (* The final Stream coalgebra *)
  Notation "'𝐒'" := (⟨⊤⟩ _ MT.S) (only parsing).
  Notation "'𝐒[' A ]" := (⟨⊤⟩ _ (@MT.S A)).
  (* Unique coalgebra morphism to the final object *)
  Notation "'⇒𝐒'" := (!-⊤ _ MT.S) (only parsing).
  Notation "[ A ']⇒𝐒'" := (@top _ MT.S A).

  (** ** Stream type and destructors **)
  Definition Stream : Type → Type := λ A ∙ ⌜𝐒[A]⌝.
  Definition head {A} : Stream A → A := λ x ∙ fst (α(𝐒) x).
  Definition tail {A} : Stream A → Stream A := λ x ∙ snd (α(𝐒) x).

  Section Bisim_Intro.

    Variables (A : Type) (R : Stream A → Stream A → Prop)
              (R_head : ∀ s₁ s₂, R s₁ s₂ → head s₁ = head s₂)
              (R_tail : ∀ s₁ s₂, R s₁ s₂ → R (tail s₁) (tail s₂)).

    (* Notation 𝑺𝒕𝒓𝒆𝒂𝒎 := (𝑺𝒕𝒓𝒆𝒂𝒎 A). *)

    Definition Rel {A} (R : A → A → Prop) := { X | R (fst X) (snd X) }.
    Definition ρ₁ {A} {R} (r : Rel R) : A := fst (projT1 r).
    Definition ρ₂ {A} {R} (r : Rel R) : A := snd (projT1 r).

    Definition 𝓡 := Rel R.

    Definition 𝓡_head₁ : 𝓡 → A := λ x ∙ (head (ρ₁ x)).
    Definition 𝓡_head₂ : 𝓡 → A := λ x ∙ (head (ρ₂ x)).
    Lemma 𝓡_head_eq : ∀ r, 𝓡_head₁ r = 𝓡_head₂ r.
    Proof.
      intros. destruct r. unfold 𝓡_head₁. unfold head. unfold ρ₁. simpl. unfold 𝓡_head₂, ρ₂. simpl.
      unfold head. now apply R_head.
    Qed.

    Definition 𝓡_tail : 𝓡 → 𝓡.
      intros. destruct X. destruct x as (s₁,s₂). exists (tail s₁ , tail s₂). apply R_tail. auto.
    Defined.

    Program Definition 𝐑 : ‵ 𝑺𝒕𝒓𝒆𝒂𝒎 A ′ := CoAlgebra.make ⦃ A ≔ 𝓡 ; α ≔ ⟨ 𝓡_head₁ , 𝓡_tail ⟩ ⦄.

    Lemma zzz : ∀ {A B} (x : A ⟨×⟩ B), x = (fst x , snd x).
    Proof.
      intros ? ? (?,?). reflexivity.
    Qed.

    (** ρ₁ is a morphism of coalgebra **)
    Program Definition θ₁ : ‵ 𝐑 ⇒ 𝐒 ′ := CoAlgebra.make ⦃ τ ≔ ρ₁ ⦄.
    Next Obligation.
      destruct x.
      unfold ρ₁. simpl. unfold 𝓡_head₁. unfold ρ₁. simpl. destruct x as (s₁,s₂). simpl in *.
      change (α(𝐒) s₁ = (head s₁ , tail s₁)).
      unfold head. unfold tail. apply zzz.
    Qed.
    Program Definition θ₂ : ‵ 𝐑 ⇒ 𝐒 ′ := CoAlgebra.make ⦃ τ ≔ ρ₂ ⦄.
    Next Obligation.
      destruct x.
      unfold ρ₂. simpl. rewrite 𝓡_head_eq. unfold 𝓡_head₁. unfold ρ₁. simpl. destruct x as (s₁,s₂). simpl in *.
      unfold head. unfold tail. apply zzz.
    Qed.

    Lemma coind : ∀ s₁ s₂, R s₁ s₂ → s₁ = s₂.
    Proof.
      intros. set (r := existT (λ X ∙ R (fst X) (snd X)) (s₁, s₂) H).
      generalize (@top_unique _ MT.S _ θ₁); intro.
      generalize (@top_unique _ MT.S _ θ₂); intro.
      assert (θ₁ ≈ θ₂). etransitivity. apply H0. symmetry. apply H1.
      simpl in H2. specialize (H2 r). unfold r in H2. unfold ρ₁,ρ₂ in H2. simpl in H2. apply H2.
    Qed.

  End Bisim_Intro.

  (** ** Corecursor on Stream and computation rules **)
  Section Corecursor.

    Context {A R : Type}.
    Variables (R_head : R → A) (R_tail : R → R).

    (* We give to R the structure of a Stream-Coalgebra *)
    Notation "⌜𝐑⌝" := R.

    Program Definition 𝐑_head : ‵ ⌜𝐑⌝ ⇒ A ′ := R_head.
    Program Definition 𝐑_tail : ‵ ⌜𝐑⌝ ⇒ ⌜𝐑⌝ ′ := R_tail.

    Program Definition 𝐑_ : ‵ 𝑺𝒕𝒓𝒆𝒂𝒎 A ′ :=
      CoAlgebra.make ⦃ A ≔ ⌜𝐑⌝ ; α ≔ ⟨ 𝐑_head , 𝐑_tail ⟩ ⦄.

    Definition corec : R → Stream A := τ [𝐑_]⇒𝐒.

  End Corecursor.

  Lemma eq_pair : ∀ {A B} (x y : A ⟨×⟩ B), x = y → fst x = fst y ∧ snd x = snd y.
  Proof.
    intros ? ? (?,?) (?,?). intros. injection H; auto.
  Qed.

  Lemma head_corec : ∀ {A R} {hd : R → A} {tl : R → R} {t}, head (corec hd tl t) = hd t.
  Proof.
    intros A R hd tl t.
    generalize (τ_commutes [𝐑_ hd tl]⇒𝐒 t); intro.
    simpl in H. unfold 𝐑_head in H. apply eq_pair in H. destruct H.
    etransitivity. apply H. reflexivity.
  Qed.

  Lemma tail_corec : ∀ {A R} {hd : R → A} {tl : R → R} {t}, tail (corec hd tl t) = corec hd tl (tl t).
  Proof.
    intros A R hd tl t.
    generalize (τ_commutes [𝐑_ hd tl]⇒𝐒 t); intro.
    simpl in H. apply eq_pair in H. destruct H.
    etransitivity. apply H0. reflexivity.
  Qed.

End Terminal_Axioms.